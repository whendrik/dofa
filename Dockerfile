FROM python:3.6

COPY app /deploy/app

RUN pip install -r /deploy/app/requirements.txt

EXPOSE 8080

WORKDIR /deploy/app

ENV FLASK_APP=app.py

CMD ["flask", "run", "-p", "8080", "-h", "0.0.0.0"]